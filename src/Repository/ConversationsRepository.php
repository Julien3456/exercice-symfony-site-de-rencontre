<?php

namespace App\Repository;

use App\Entity\Conversations;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Conversations|null find($id, $lockMode = null, $lockVersion = null)
 * @method Conversations|null findOneBy(array $criteria, array $orderBy = null)
 * @method Conversations[]    findAll()
 * @method Conversations[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ConversationsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Conversations::class);
    }

     /**
      * @return Conversations[] Returns an array of Conversations objects
      */

    public function findConversation($sender, $receiver): ?array
    {

        $em = $this->getEntityManager();
        $query = $em->createQuery(
            'SELECT c
            FROM App\Entity\Conversations c
            WHERE (c.senders = :sender AND c.receivers = :receiver)
            OR (c.senders = :receiver AND c.receivers = :sender)'
        );
        $query->setParameter('sender', $sender);
        $query->setParameter('receiver', $receiver);
//        dd($query->getResult());
        if ($query->getResult()) { return $query->getResult(); }
        else { return []; }
    }

    public function findAllConversation($user): ?array
    {

        $em = $this->getEntityManager();
        $query = $em->createQuery(
            'SELECT c
            FROM App\Entity\Conversations c
            WHERE (c.senders = :user OR c.receivers = :user)'
        );
        $query->setParameter('user', $user);
//        dd($query->getResult());
        if ($query->getResult()) { return $query->getResult(); }
        else { return []; }
    }

    // /**
    //  * @return Conversations[] Returns an array of Conversations objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Conversations
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
