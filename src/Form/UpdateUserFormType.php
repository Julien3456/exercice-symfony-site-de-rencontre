<?php

namespace App\Form;

use App\Entity\City;
use App\Entity\Genre;
use App\Entity\Hair;
use App\Entity\Interest;
use App\Entity\Search;
use App\Entity\User;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\File;

class UpdateUserFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)

    {

        $builder
            ->add('username')
            ->add('mail', EmailType::class, [
                'required' => false,
            ])
            ->add('dob', DateType::class, [
                'required' => true,
                'widget' => 'single_text',
                'format' => 'yyyy-MM-dd',
            ])
            ->add('description', TextareaType::class, [
                'required' => false,
            ])
            ->add('avatar', FileType::class, [
                'data_class' => null,
//                'mapped' => false,
                'label' => 'Product Picture',
                'required' => false,
                'constraints' =>
                [
                    new File([
                        'maxSize' => '2M',
                        'mimeTypes' =>
                            [
                                'image/*',
//                                'image/jpg',
                            ], 'mimeTypesMessage' => 'Please upload a valid picture!',
                    ])
                ],
            ])
            ->add('genre', EntityType::class, [
                'class' => Genre::class,
                'choice_label' => 'genreType',
                'required' => false,
            ])
            ->add('newGenre', TextType::class, [
                'mapped' => false,
                'required' => false,
                'attr' => [
                    'placeholder' => 'Add a new genre'
                ]
            ])

            ->add('hair', EntityType::class, [
                'class' => Hair::class,
                'choice_label' => 'color',
                'required' => false,
            ])
            ->add('newHair', TextType::class, [
                'mapped' => false,
                'required' => false,
                'attr' => [
                    'placeholder' => 'Add a new hair'
                ]
            ])

            ->add('searchGenre', EntityType::class, [
                'class' => Genre::class,
                'choice_label' => 'genreType',
                'required' => true,
            ])
            ->add('searchAgeMin', IntegerType::class, [
                'attr' => [
                    'min' => 18,
                    'max' => 99
                ],
                'required' => false,
                'mapped' => false
            ])
            ->add('searchAgeMax', IntegerType::class, [
                'attr' => [
                    'min' => 18,
                    'max' => 99
                ],
                'required' => false,
                'mapped' => false
            ])

            ->add('city', EntityType::class, [
                'class' => City::class,
                'choice_label' => 'name',
                'required' => false,
            ])
            ->add('newCity', TextType::class, [
                'mapped' => false,
                'required' => false,
                'attr' => [
                    'placeholder' => 'Add a new city'
                ]
            ])
            ->add('interestUsers', EntityType::class, [
                'class' => Interest::class,
                'choice_label' => 'name',
//                'choice_name' => ChoiceList::
                'required' => false,
                'mapped' => false,
                'placeholder' => 'Add an interest',

                'attr' => [
                    'placeholder' => 'Add an interest'
                ]
            ])
            ->add('newInterestUsers', TextType::class, [
                'mapped' => false,
                'required' => false,
                'attr' => [
                    'placeholder' => 'Add a new interest'
                ]
            ]);
    }
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
