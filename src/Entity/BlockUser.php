<?php

namespace App\Entity;

use App\Repository\BlockUserRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=BlockUserRepository::class)
 */
class BlockUser
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="blockedUsers")
     * @ORM\JoinColumn(nullable=false)
     */
    private $blocked;

    /**
     * @ORM\ManyToOne(targetEntity=User::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $blocking;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getblocked(): ?User
    {
        return $this->blocked;
    }

    public function setblocked(?User $blocked): self
    {
        $this->blocked = $blocked;

        return $this;
    }

    public function getblocking(): ?User
    {
        return $this->blocking;
    }

    public function setblocking(?User $blocking): self
    {
        $this->blocking = $blocking;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }
}
