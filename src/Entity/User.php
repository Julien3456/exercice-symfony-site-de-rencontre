<?php

namespace App\Entity;

use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=UserRepository::class)
 * @UniqueEntity(fields={"username"}, message="There is already an account with this username")
 */
class User implements UserInterface
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     */
    private $username;

    /**
     * @ORM\Column(type="json")
     */
    private $roles = [];

    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     */
    private $password;

    /**
     * @ORM\Column(type="string", length=255, nullable=true, unique=true)
     */
    private $mail;

    /**
     * @ORM\Column(type="date", nullable=false)
     */
    private $dob;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $avatar;

    private $dirAvatar;
    private $age;
    private $interests;


    /**
     * @ORM\ManyToOne(targetEntity=Hair::class, inversedBy="users")
     * @ORM\JoinColumn(nullable=true)
     */
    private $hair;

    /**
     * @ORM\ManyToOne(targetEntity=City::class, inversedBy="users")
     * @ORM\JoinColumn(nullable=true)
     */
    private $city;


    /**
     * @ORM\OneToMany(targetEntity=InterestUser::class, mappedBy="user", orphanRemoval=true)
     */
    private $interestUsers;

    /**
     * @ORM\ManyToOne(targetEntity=Genre::class)
     * @ORM\JoinColumn(nullable=true)
     */
    private $genre;

    /**
     * @ORM\ManyToOne(targetEntity=Genre::class)
     * @ORM\JoinColumn(nullable=false)
     */
    private $searchGenre;

    /**
     * @ORM\OneToMany(targetEntity=Conversations::class, mappedBy="senders", orphanRemoval=true)
     */
    private $conversations;

    /**
     * @ORM\OneToMany(targetEntity=BlockUser::class, mappedBy="blocking", orphanRemoval=true)
     */
    private $blockingUsers;

    /**
     * @ORM\OneToMany(targetEntity=Messages::class, mappedBy="senders", orphanRemoval=true)
     */
    private $messages;

    /**
     * @ORM\OneToOne(targetEntity=Search::class, mappedBy="user", cascade={"persist", "remove"})
     */
    private $search;

    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    private $postalCode;

//    /**
//     * @ORM\ManyToOne(targetEntity=Search::class, inversedBy="user")
//     */
//    private $search;


    public function __construct()
    {
        $this->interests = new ArrayCollection();
        $this->conversations = new ArrayCollection();
        $this->blockingUsers = new ArrayCollection();
        $this->messages = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function __toString(): string
    {
        return $this->username;
    }
    public function getUsername(): string
    {
        return (string) $this->username;
    }

    public function setUsername(string $username): self
    {
        $this->username = $username;
        return $this;
    }

    public function getDirAvatar(): string
    {
        return (string) $this->dirAvatar;
    }

    public function setDirAvatar(string $dir): self
    {
        $this->dirAvatar = $dir;
        return $this;
    }
    public function getAge(): int
    {
        if($this->getDob()) {
            $dateNow = new \DateTime('now');
            $age = $dateNow->diff($this->getDob());
            $ageFormat = $age->format("%y");
        } else { $ageFormat = 0; }
        return (int) $ageFormat;
    }

    public function setAge(int $age): self
    {
        $this->age = $age;
        return $this;
    }


    public function getInterests(): ?string
    {
        if ($this->interests) {
            return (string)implode(', ', $this->interests);
        } else {return null;}
    }

    public function setInterests(array $interests): self
    {
        $this->interests = $interests;
        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return (string) $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Returning a salt is only needed, if you are not using a modern
     * hashing algorithm (e.g. bcrypt or sodium) in your security.yaml.
     *
     * @see UserInterface
     */
    public function getSalt(): ?string
    {
        return null;
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function getMail(): ?string
    {
        return $this->mail;
    }

    public function setMail(string $mail): self
    {
        $this->mail = $mail;

        return $this;
    }

    public function getDob(): ?\DateTimeInterface
    {
        return $this->dob;
    }

    public function setDob(\DateTimeInterface $dob): self
    {
        $this->dob = $dob;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getCity(): ?City
    {
        return $this->city;
    }

    public function setCity(?City $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getAvatar(): ?string
    {
        return $this->avatar;
    }

    public function setAvatar(?string $avatar): self
    {
        $this->avatar = $avatar;

        return $this;
    }

    public function getHair(): ?Hair
    {
        return $this->hair;
    }

    public function setHair(?Hair $hair): self
    {
        $this->hair = $hair;

        return $this;
    }

    /**
     * @return Collection|InterestUser[]
     */
    public function getInterestUsers(): Collection
    {
        return $this->interestUsers = new ArrayCollection();
    }

    public function addInterestUser(InterestUser $interestUser): self
    {
//        if (!$this->interestUsers->contains($interestUser)) {
            $this->interestUsers[] = $interestUser;
            $interestUser->setUser($this);
//        }

        return $this;
    }

    public function removeInterestUser(InterestUser $interestUser): self
    {
        if ($this->interestUsers->removeElement($interestUser)) {
            // set the owning side to null (unless already changed)
            if ($interestUser->getUser() === $this) {
                $interestUser->setUser(null);
            }
        }

        return $this;
    }

    public function getGenre(): ?Genre
    {
        return $this->genre;
    }

    public function setGenre(?Genre $genre): self
    {
        $this->genre = $genre;

        return $this;
    }

    public function getSearchGenre(): ?Genre
    {
        return $this->searchGenre;
    }

    public function setSearchGenre(?Genre $searchGenre): self
    {
        $this->searchGenre = $searchGenre;

        return $this;
    }

    /**
     * @return Collection|Conversations[]
     */
    public function getConversations(): Collection
    {
        return $this->conversations;
    }

    public function addConversation(Conversations $conversation): self
    {
        if (!$this->conversations->contains($conversation)) {
            $this->conversations[] = $conversation;
            $conversation->setSenders($this);
        }

        return $this;
    }

    public function removeConversation(Conversations $conversation): self
    {
        if ($this->conversations->removeElement($conversation)) {
            // set the owning side to null (unless already changed)
            if ($conversation->getSenders() === $this) {
                $conversation->setSenders(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|BlockUser[]
     */
    public function getBlockingUsers(): Collection
    {
        return $this->blockingUsers;
    }

    public function addBlockingUser(BlockUser $blockingUser): self
    {
        if (!$this->blockingUsers->contains($blockingUser)) {
            $this->blockingUsers[] = $blockingUser;
            $blockingUser->setBlocking($this);
        }

        return $this;
    }

    public function removeBlockingUser(BlockUser $blockingUser): self
    {
        if ($this->blockingUsers->removeElement($blockingUser)) {
            // set the owning side to null (unless already changed)
            if ($blockingUser->getBlocking() === $this) {
                $blockingUser->setBlocking(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Messages[]
     */
    public function getMessages(): Collection
    {
        return $this->messages;
    }

    public function addMessage(Messages $message): self
    {
        if (!$this->messages->contains($message)) {
            $this->messages[] = $message;
            $message->setSenders($this);
        }

        return $this;
    }

    public function removeMessage(Messages $message): self
    {
        if ($this->messages->removeElement($message)) {
            // set the owning side to null (unless already changed)
            if ($message->getSenders() === $this) {
                $message->setSenders(null);
            }
        }

        return $this;
    }

    public function getSearch(): ?Search
    {
        return $this->search;
    }

    public function setSearch(Search $search): self
    {
        // set the owning side of the relation if necessary
        if ($search->getUser() !== $this) {
            $search->setUser($this);
        }

        $this->search = $search;

        return $this;
    }

    public function getPostalCode(): ?string
    {
        return $this->postalCode;
    }

    public function setPostalCode(?string $postalCode): self
    {
        $this->postalCode = $postalCode;

        return $this;
    }
}
