<?php

namespace App\Entity;

use App\Repository\GenreRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=GenreRepository::class)
 */
class Genre
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $genreType;


    public function __construct()
    {
        $this->users = new ArrayCollection();
    }

    public function __toString():string
    {
        return $this->genreType;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getGenreType(): ?string
    {
        return $this->genreType;
    }

    public function setGenreType(string $genreType): self
    {
        $this->genreType = $genreType;

        return $this;
    }
}
