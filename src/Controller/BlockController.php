<?php

namespace App\Controller;

use App\Entity\BlockUser;
use App\Entity\Conversations;
use App\Entity\User;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/block")
 */
class BlockController extends AbstractController
{
    /**
     * @Route("/block/{id}", name="block")
     * @param Request $request
     * @param $id
     * @return Response
     */
    public function block(Request $request, $id): Response
    {
        $this->denyAccessUnlessGranted('ROLE_USER');
        $session = new Session();
        // Création d'une variable user si un user est connecté
        $user = $this->getUser();

        // Récupération de l'adresse précédente
        $referer = explode('/', $request->server->get('HTTP_REFERER'))[3];

        // Récupération de l'instance du user blocké
        $blocked = $this->getDoctrine()->getRepository(User::class)->findOneBy(['id' => $id]);

        // Création d'une nouvelle instance $block
        $block = new BlockUser();

        // Enregistrement dans l'instance du bloqué, bloquant, et la date de création
        $block->setblocked($blocked);
        $block->setblocking($user);
        $block->setCreatedAt(new \DateTime('now'));

        // Appel à la fonction de persistance
        $this->persisted($block);

        // Création d'une nouvelle session d'utilisateurs bloqués
        $session->set('blocked', $this->getDoctrine()->getRepository(BlockUser::class)->findBy(['blocking' => $user->getId()]));
        // Redirection vers la liste des conversations
        if ($referer == 'conversations') {
            return $this->redirectToRoute('conversations_show', ['id' => $user->getId()]);
        } else {
            return $this->redirectToRoute('main');
        }
    }
    /**
     * @Route("/view", name="blockListView")
     */
    public function blockListView(): Response
    {
        $user = $this->getUser();
        $listUsersBlocking = $this->getDoctrine()->getRepository(BlockUser::class)->findBy(['blocking' => $user->getId()]);

        return $this->render('block/viewListBlock.html.twig', [
            'listUsernamesBlocking' => $listUsersBlocking,
            'user' => $user,
        ]);
    }

    /**
     * @Route("/del/{id}", name="del_block"), methods={"DELETE"}
     * @param $id
     * @param Request $request
     * @return Response
     */
    public function del_block(Request $request, $id): Response
    {
        $this->denyAccessUnlessGranted('ROLE_USER');
        $session = new Session;

    // Création d'une variable contenant les utilisateurs bloqués
        $listBlocked = $session->get('blocked');
    // Variable utilisateur
        $user = $this->getUser();
    // Variable de l'utilisateur qui va être débloqué
        $block = $this->getDoctrine()->getRepository(BlockUser::class)->findOneBy(['id' => $id]);

    // Si l'utilisateur connecté est l'utilisateur bloquant
        if ($block->getBlocking()->getId() == $user->getId()){
    // Si les clés de sécurité sont valides
            if ($this->isCsrfTokenValid('delete'.$block->getId(), $request->request->get('_token'))) {

    // Pour chaques utilisateurs bloqués
                foreach ($listBlocked as $blocked){
    // Si l'utilisateur débloqué de la liste est l'utilisateur a débloqué
                    if ($blocked->getId() == $id){
    // Il sort de la session de bloquage
                        unset($session->get('blocked')[array_search($blocked,$session->get('blocked'))]);
                    }
                }
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->remove($block);
                $entityManager->flush();
            }
        }

        return $this->redirectToRoute('blockListView');
    }

    // Fonction pour persister
    private function persisted($user)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($user);
        $entityManager->flush();
    }
}