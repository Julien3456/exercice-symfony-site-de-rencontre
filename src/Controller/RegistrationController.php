<?php

namespace App\Controller;

use App\Entity\BlockUser;
use App\Entity\City;
use App\Entity\Genre;
use App\Entity\Hair;
use App\Entity\Interest;
use App\Entity\InterestUser;
use App\Entity\Search;
use App\Entity\User;
use App\Form\RegistrationFormType;
use App\Form\UpdateUserFormType;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response as ResponseAlias;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;


class RegistrationController extends AbstractController
{
    /**
     * @Route("/register", name="app_register")
     * @param Request $request
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @return ResponseAlias
     */
    public function register(Request $request, UserPasswordEncoderInterface $passwordEncoder): ResponseAlias
    {
        $user = new User();
        $search = new Search();
        $form = $this->createForm(RegistrationFormType::class, $user);
        $form->handleRequest($request);
//dd($form);
        if ($form->isSubmitted() && $form->isValid()) {
            if ($user->getAge() < 18 ){
                return $this->redirectToRoute('main');
            }
            $user->setPassword(
                $passwordEncoder->encodePassword(
                    $user,
                    $form->get('plainPassword')->getData()
                )
            );
            $search->setAgeMin(18);
            $search->setAgeMax(100);
            $user->setSearch($search);

//            $this->persisted($search);
            $this->persisted($user);

            return $this->redirectToRoute('app_login');
        }

        return $this->render('registration/register.html.twig', [
            'registrationForm' => $form->createView(),
            'user' => $user,
        ]);
    }

    /**
     * @Route("view/updateView/{id}", name="app_updateView")
     * @param Request $request
     * @param $id
     * @return ResponseAlias
     * @throws Exception
     */
    public function updateView(Request $request, $id): ResponseAlias
    {
        $this->denyAccessUnlessGranted('ROLE_USER');

// Création d'une variable user avec son ID si un user est connecté
        if ($this->getUser()){
            $user = $this->getUser()->getId();
        } else { $user = 0; }
//        dd($request);

// Recherche du user sélectionné
        $user = $this->getDoctrine()->getRepository(User::class)->find($id);
        $searchUser = $this->getDoctrine()->getRepository(Search::class)->findOneBy(['user' => $user]);
//        $user
//        dd($form);

// Création du formulaire de update
        $form = $this->createForm(UpdateUserFormType::class, $user);
//        dd($form->getViewData());

// Récupération du formaulaire
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

// l'utilisateur sélectionne une date de naissance
            if ($request->request->get('update_user_form')['dob'] != "") {
                $user->setDob(new \DateTime($request->request->get('update_user_form')['dob']));
                $this->persisted($user);
            }

// Pour la caractéristique cheveux
    // Si l'utilisateur sélectionne une nouvelle caractéristique
            if ($request->request->get('update_user_form')['newHair'] != "") {
                // On récupère la nouvelle valeur
                $responseForm = $request->request->get('update_user_form')['newHair'];

                // On vérifie si la valeur existe déjà dans la DB
                $responseRepository = $this->getDoctrine()->getRepository(Hair::class)->findOneBy(['color' => $responseForm]);

                // Si elle n'existe pas, on enregistre la nouvelle valeur dans l'entitée
                if(!$responseRepository){
                    $result = $this->newCharacteristic($request, 'newHair', new Hair(), 'setColor');
                    $user->setHair($result);
                } else {

                // Sinon on récupère la valeur et on la persiste dans l'utilisateur
                    $user->setHair($responseRepository);
                }
                $this->persisted($user);
            }

// Pour la caractéristique genre
    // Si l'utilisateur sélectionne une nouvelle caractéristique
            if ($request->request->get('update_user_form')['newGenre'] != "") {
                $responseForm = $request->request->get('update_user_form')['newGenre'];
                $responseRepository = $this->getDoctrine()->getRepository(Genre::class)->findOneBy(['genreType' => $responseForm]);
                if (!$responseRepository) {
                    $result = $this->newCharacteristic($request, 'newGenre', new Genre(), 'setGenreType');
                    $user->setGenre($result);
                } else {
                    $user->setGenre($responseRepository);
                }
                $this->persisted($user);
            }

// Pour la caractéristique city
    // Si l'utilisateur sélectionne une nouvelle caractéristique
            if ($request->request->get('update_user_form')['newCity'] != "") {
                $responseForm = $request->request->get('update_user_form')['newCity'];
                $responseRepository = $this->getDoctrine()->getRepository(City::class)->findOneBy(['name' => $responseForm]);
                if (!$responseRepository){
                    $result = $this->newCharacteristic($request, 'newCity', new City(), 'setName');
                    $user->setCity($result);

                } else {
                    $user->setCity($responseRepository);
                }
                $this->persisted($user);
//                dd($user);
            }

// Pour un avatar
    // Si l'utilisateur sélectionne un fichier image
            $picture = $form->get('avatar')->getData();
            if ($request->files->get('update_user_form')['avatar'] != "") {
                $avatarFolderDest = $this->getParameter('picture_dir').'/'.$user->getId().'/'.'avatar';
                $newNamePicture = md5(uniqid(rand())) . "." . $picture->guessExtension();
                $picture->move($avatarFolderDest, $newNamePicture);
                $user->setAvatar($newNamePicture);
                $this->persisted($user);
            }

// Pour la caractéristique centre d'interet
    // Si l'utilisateur sélectionne une nouvelle caractéristique
            if ($request->request->get('update_user_form')['newInterestUsers'] != "") {
                $responseForm = $request->request->get('update_user_form')['newInterestUsers'];
                $responseRepository = $this->getDoctrine()->getRepository(Interest::class)->findOneBy(['name' => $responseForm]);
                if (!$responseRepository){
                    $result = $this->newCharacteristic($request, 'newInterestUsers', new Interest(), 'setName');
                    $newInterestUser = new InterestUser();
                    $newInterestUser->setInterest($result);
                    $newInterestUser->setUser($user);
                    $this->persisted($newInterestUser);
//                    $this->persisted($user);
                }
                $this->persisted($user);
            }
            if ($request->request->get('update_user_form')['interestUsers'] != ""){
                $responseForm = $request->request->get('update_user_form')['interestUsers'];
                $responseRepository = $this->getDoctrine()->getRepository(Interest::class)->findOneBy(['id' => $responseForm]);

                $newInterestUser = new InterestUser();
                $newInterestUser->setInterest($responseRepository);
                $newInterestUser->setUser($user);
                $this->persisted($newInterestUser);
            }
// Création d'un objet search à utiliser si les champs de recherche age min et max sont renseignés
            if ($request->request->get('update_user_form')['searchAgeMin'] != "") {
                $searchAge = $this->getDoctrine()->getRepository(Search::class)->findOneBy(['user' => $user->getId()]);
                if ($searchAge){
                    $searchAge->setAgeMin($request->request->get('update_user_form')['searchAgeMin']);
                    $this->persisted($searchAge);
                } else {
                    $searchAge = new Search();
                    $searchAge->setAgeMin($request->request->get('update_user_form')['searchAgeMin']);
                    $user->setSearch($searchAge);
                    $this->persisted($searchAge);
                }
            }
            if ($request->request->get('update_user_form')['searchAgeMax'] != "") {
                $searchAge = $this->getDoctrine()->getRepository(Search::class)->findOneBy(['user' => $user->getId()]);
                if ($searchAge){
                    $searchAge->setAgeMax($request->request->get('update_user_form')['searchAgeMax']);
                    $this->persisted($searchAge);
                } else {
                    $searchAge = new Search();
                    $searchAge->setAgeMax($request->request->get('update_user_form')['searchAgeMax']);
                    $user->setSearch($searchAge);
                    $this->persisted($searchAge);
                }
            }

            return $this->redirectToRoute('view', ['id' => $user->getId()]);
        }

// On envoie le formulaire à la vue
        return $this->render('registration/update.html.twig', [
            'updateForm' => $form->createView(),
            'user' => $user,
        ]);
    }

// Fonction pour persister
    private function persisted($user)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($user);
        $entityManager->flush();
    }

// Fonction pour enregistrer la nouvelle caractéristique
    private function newCharacteristic($request, $inputCharacteristics, $entity, $setterEntity)
    {
        $newCharacteristicEntity = new $entity;
        $newCharacteristicEntity->$setterEntity($request->request->get('update_user_form')[$inputCharacteristics]);
        $this->persisted($newCharacteristicEntity);
        return $newCharacteristicEntity;
    }
}
