<?php

namespace App\Controller;

use App\Entity\BlockUser;
use App\Entity\InterestUser;
use App\Entity\Search;
use App\Entity\User;
use App\Form\SearchFormType;
use App\Form\SearchType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Annotation\Route;

class MainController extends AbstractController
{
    /**
     * @Route("/", name="main")
     * @param Request $request
     * @return Response
     */

    // Affichage dans main de tous les utilisateurs sauf l'utilisateur connectés
    public function main(Request $request): Response
    {
    // Création d'une session
        $session = new Session();
        $session->start();

    // Création d'un tableau pour stocker les utilisateurs bloqués par l'utilisateur connecté
        $listBlocked = [];


    // Si un utilisateur est connecté
        if ($this->getUser()) {
    // Récupération de l'utilisateur connecté
            $user = $this->getUser();

//dd($user->getSearch());
    // Récupération de la liste des utilisateurs bloqués
            $listBlocked = $this->getDoctrine()->getRepository(BlockUser::class)->findBy(['blocking' => $user->getId()]);
    // Et stockage de la liste dans la variable session
            $session->set('blocked', $listBlocked);

    // Récupération les utilisateurs par rapport au genre recherché et la tranche d'age
        $matchers = $this->getDoctrine()->getRepository(User::class)->findMatcherFilted($user->getSearch()->getAgeMin(), $user->getSearch()->getAgeMax(), $user->getSearchGenre()->getId());


    // Si l'utilisateur n'est pas connecté
        } else {
    // Initialisation de $user
            $user = 0;
    // Récupération de tous les utilisateurs
            $matchers = $this->getDoctrine()->getRepository(User::class)->findAll();
        }



    // Création d'une variable genre pour l'utilisation dans le fichier twig
        $labelGenre = "utilisateurs(trices)";

    // Pour chaque utilisateurs
        foreach ($matchers as $matcher) {
            if ($listBlocked) {
                foreach ($listBlocked as $block) {
                    //                dd($matcher->getId());
                    if ($block->getBlocked()->getId() == $matcher->getId()) {
                        unset($matchers[array_search($matcher, $matchers)]);
                    }
                }
            }
    // Si l'utilisateur connecté est l'utilisateur récupéré
            if ($user) {
                if ($user->getId() == $matcher->getId()) {
                    // Il sort du tableau
                    unset($matchers[array_search($matcher, $matchers)]);
                }

                if ($user->getSearchGenre()->getId() == 1 ) { $labelGenre = "utilisateurs"; }
                elseif ($user->getSearchGenre()->getId() == 2) { $labelGenre = "utilisatrices"; }
            }
    // Enregistrement des infos dans l'utilisateur
            $this->InfoUser($matcher);
        }
        return $this->render('main/main.html.twig', [
            'matchers' => $matchers,
            'user' => $user,
            'utilised' => $labelGenre,
        ]);
    }

    /**
     * @Route("/view/{id}", name="view")
     * @param $id
     * @return Response
     */
    public function view($id): Response
    {
        $this->denyAccessUnlessGranted('ROLE_USER');
        $matcher = $this->getDoctrine()->getRepository(User::class)->findOneBy(['id' => $id]);
        $matcher = $this->InfoUser($matcher);

//        dd($matcher);
        if ($this->getUser()){
            $user = $this->getUser()->getId();
        } else { $user = 0; }
//dd($matcher->getId());
        return $this->render('main/view.html.twig', [
            'matcher' => $matcher,
            'user' => $user,
        ]);
    }

    private function InfoUser($matcher): User
    {
        // Enregistrement du chemin des pictures et avatar
        $matcher->setDirAvatar('Uploads/Pictures/' . $matcher->getId() . '/avatar/');
        // Enregistrement de l'age avec la dob
        $age = $matcher->getAge();
        $matcher->setAge($age);
        // Si il n'y a pas d'avatar enregistré, enregistrement d'un avatar par defaut
        if (!$matcher->getAvatar()) {
            $matcher->setDirAvatar('Uploads/Pictures/Default/');
            if ($matcher->getGenre()) {
                $idGenre = $matcher->getGenre()->getId();
                switch ($idGenre) {
                    case 1:
                        $matcher->setAvatar('male2.png');
                        break;
                    case 2:
                        $matcher->setAvatar('female.png');
                        break;
                    case 3:
                        $matcher->setAvatar('default.png');
                        break;
                }
            } else {
                $matcher->setAvatar('default.png');
            }
        }

        // Récupération des centre d'interets de l'utilisateur sur la table intermédiaire
        $tabValueInterests = [];
        $valueInterests = $this->getDoctrine()->getRepository(InterestUser::class)->findBy(['user' => $matcher->getId()]);
        // si il y a centre d'interet
        if ($valueInterests) {
            // Pour chaque centre d'interet, stockage dans un tableau
            foreach ($valueInterests as $valueInterest) {
                $tabValueInterests[] .= $valueInterest->getInterest()->getName();
            }
            // Et enregistrement dans l'utilsateur
            $matcher->setInterests($tabValueInterests);
        }
//        dd($matcher);
        return $matcher;
    }
}
