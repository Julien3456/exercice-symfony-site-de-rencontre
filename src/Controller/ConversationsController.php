<?php

namespace App\Controller;

use App\Entity\BlockUser;
use App\Entity\Conversations;
use App\Entity\Messages;
use App\Entity\User;
use Symfony\Component\HttpFoundation\Session\Session;
use App\Form\ConversationsType;
use App\Repository\ConversationsRepository;
use Doctrine\ORM\Mapping\Id;
use phpDocumentor\Reflection\Exception\PcreException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/conversations")
 */
class ConversationsController extends AbstractController
{
    /**
     * @Route("/{id}", name="conversations_index")
     * @param Request $request
     * @param ConversationsRepository $conversationsRepository
     * @param id //receiverId
     * @return Response
     */
    public function index(Request $request, ConversationsRepository $conversationsRepository, $id): Response
    {
        $this->denyAccessUnlessGranted('ROLE_USER');


    // Création d'une variable user avec son ID si un user est connecté
        $user = $this->getUser();

    // Récupération du destinataire avec l'id passé en paramètre
        $receiver = $this->getDoctrine()->getRepository(User::class)->findOneBy(['id' => $id]);

    // Essai de récupération d'une conversation
        $conversation = $this->getDoctrine()->getRepository(Conversations::class)->findConversation($user->getId(), $receiver->getId());

    // Si la conversation existe
        if ($conversation) {
    // récupération de la conversation sans le tableau
            $conversation = $conversation[0];
    // Récupération des messages de la conversation
            $messages = $this->getDoctrine()->getRepository(Messages::class)->findBy(['conversation' => $conversation->getId()], ['createdAt' => 'DESC']);
        } else { $messages = []; }
    // Indexation des messages avec $i
        $i = 0;
        foreach ($messages as $message){
//            dd($message->getSenders());
            // Si c'est le dernier message de la conversation et le premier de la liste
            if ($i == 0 && $user->getId() != $message->getSenders()->getId()) {
                $message->setReadAt(new \DateTime('now'));
                $this->getDoctrine()->getManager()->flush();
            }
            if ($conversation->getSenders()->getId() == $message->getSenders()->getId()) {
                $message->setClassMessage('sender');
            } else { $message->setClassMessage('receiver'); }
            $i++;
        }
//    dd($messages);

        return $this->render('conversations/index.html.twig', [
            'messages' => $messages,
            'user' => $user,
            'receiver' => $receiver,
            'conversation' => $conversation,
        ]);
    }

    /**
     * @Route("/new/{id}/{conv}", name="conversations_new")
     * @param Request $request
     * @param $id
     * @param $conv
     * @return Response
     */
    public function new(Request $request, $id, $conv): Response
    {
        $this->denyAccessUnlessGranted('ROLE_USER');

    // Récupération du destinataire avec l'id passé en paramètre
        $receiver = $this->getDoctrine()->getRepository(User::class)->findOneBy(['id' => $id]);

    // Création d'une variable user avec son ID si un user est connecté
        $user = $this->getUser();

    // Si il n'y a pas de conversation existante
        if ($conv == 0) {
    // Création d'une nouvelle conversation
            $conversation = new Conversations();
    // Enregistrement de l'expéditeur, destinataire, et date de création
            $conversation->setSenders($this->getUser());
            $conversation->setReceivers($receiver);
            $conversation->setCreatedAt(new \DateTime('now'));

    // Sinon récupération de la consation en cours
        } else {
            $conversation = $this->getDoctrine()->getRepository(Conversations::class)->findConversation($user, $receiver->getId())[0];
        }

    // Création du nouveau message
        $message = new Messages();
        $form = $this->createForm(ConversationsType::class, $conversation);
        $form->handleRequest($request);

    // Quand le formulaire est renseigné
        if ($form->isSubmitted() && $form->isValid()) {
    // Enregistrement du contenu du message, et de la date de création
            $message->setSenders($user);
            $message->setContent($request->request->get('conversations')['message']);
            $message->setCreatedAt(new \DateTime('now'));
//dd($message);
    // Enregistrement du message dans la conversation
            $conversation->addMessage($message);

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($conversation);
            $entityManager->persist($message);
            $entityManager->flush();

            return $this->redirectToRoute('conversations_index', ['id'=>$receiver->getId()]);
        }

        return $this->render('conversations/new.html.twig', [
            'conversation' => $conversation,
            'form' => $form->createView(),
            'user' => $user,
            'receiver' => $receiver,
        ]);
    }

    /**
     * @Route("/show/{id}", name="conversations_show")
     * @param $id
     * @return Response
     */

    public function show($id): Response
    {
        $this->denyAccessUnlessGranted('ROLE_USER');
    // Création d'une variable user avec son ID si un user est connecté
        $user = $this->getUser();
        $lastMessages = [];

        // Récupération de la liste des utilisateurs bloqués
        $listBlocked = $this->getDoctrine()->getRepository(BlockUser::class)->findBy(['blocking' => $user->getId()]);
    // Et stockage de la liste dans la variable session
        $session = new Session();
        $session->set('blocked', $listBlocked);

    // Création d'une variable avec un tableau des conversation en cours de l'utilisateur connecté
        $conversations = $this->getDoctrine()->getRepository(Conversations::class)->findAllConversation($user->getId());

    // Pour chaque conversation
        foreach ($conversations as $conversation){
    // Si il y a une liste d'utilisateur bloqué
            if ($listBlocked){
    // Pour chaque utilisateur bloqués
                foreach ($listBlocked as $blocked){
    // Si l'utilisateur bloqué est un des destinataires ou expéditeurs de la conversation de la boucle
                    if ($conversation->getSenders()->getId() == $blocked->getBlocked()->getId() || $conversation->getReceivers()->getId() == $blocked->getBlocked()->getId()){
    // Retrait de la conversation de la liste des conversations pour qu'elle ne soit plus accessible
                        unset($conversations[array_search($conversation, $conversations)]);
                    }
                }
            }
    // Si le destinataire est l'utilisateur connecté
//            dd($conversation->getSenders());
            if ($conversation->getReceivers()->getId() == $user->getId()){
    // Enregistrement de l'identifiant de l'expediteur dans l' attribut "$whoUserRead"
                $conversation->setWhoUserRead($conversation->getSenders());
    // Sinon enregistrement de l'identifiant de l'utilisateur connecté dans l' attribut "$whoUserRead"
            } else { $conversation->setWhoUserRead($conversation->getReceivers()); }
//            $countMessage = count($this->getDoctrine()->getRepository(Messages::class)->findBy(['conversation' => $conversation->getId()]));
            $lastMessage = $this->getDoctrine()->getRepository(Messages::class)->findBy(['conversation' => $conversation->getId()], ['createdAt' => 'DESC'], 1);
//            $DateMessage1day = $lastMessage[0]->getReadAt()->modify('+1 day');
            if ($lastMessage[0]->getReadAt()) {
                $DateMessage1day = $lastMessage[0]->getReadAt()->add(new \DateInterval('P1D'));
            }

            $lastMessage[0]->setDateMessage1day($DateMessage1day);

            $lastMessages[]=$lastMessage;

        }
//        dd($lastMessages);

        return $this->render('conversations/show.html.twig', [
            'conversations' => $conversations,
            'user' => $user,
            'lastMessages' => $lastMessages,
        ]);
    }

    /**
     * @Route("/del/{id}", name="conversations_delete", methods={"DELETE"})
     * @param Request $request
     * @param $id
     * @return Response
     */
    public function delete(Request $request, $id): Response
    {
        $this->denyAccessUnlessGranted('ROLE_USER');
        $user = $this->getUser();
        $conversation = $this->getDoctrine()->getRepository(Conversations::class)->findOneBy(['id' => $id]);
        if ($conversation->getSenders()->getId() == $user->getId() || $conversation->getReceivers()->getId() == $user->getId()){

            if ($this->isCsrfTokenValid('delete'.$conversation->getId(), $request->request->get('_token'))) {
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->remove($conversation);
                $entityManager->flush();
            }
        }
        return $this->redirectToRoute('conversations_show', ['id'=>$user->getId()]);
    }
}
